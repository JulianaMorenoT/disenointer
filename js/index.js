$(function(){
        $("[data-toggle='tooltipe']").tooltip();
        $("[data-toggle='popover']").popover();
        $('.carousel').carousel({
          interval: 2000
        });
  
        $('#contacto').on('show.bs.modal', function (e){
          console.log("El modal se esta mostrando");
          $('#contactoBtn').removeClass('btn-outline-secondary');
          $('#contactoBtn').addClass('btn-primary');
          $('#contactoBtn').prop('disabled', true); 
  
        }); 
        $('#contacto').on('shown.bs.modal', function (e){
          console.log("El modal se mostró");
        });
        $('#contacto').on('hide.bs.modal', function (e){
          console.log("El modal se está ocultando");
        });
        $('#contacto').on('hidden.bs.modal', function (e){
          console.log("El modal se ocultó");
          $('#contactoBtn').removeClass('btn-primary');
          $('#contactoBtn').addClass('btn-outline-secondary');
          $('#contactoBtn').prop('disabled', false); 
        });
});